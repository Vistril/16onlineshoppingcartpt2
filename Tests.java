import org.junit.*;
import static org.junit.Assert.*;
import java.io.*;

public class Tests {
  ShoppingCartManager student = new ShoppingCartManager();
  private ByteArrayOutputStream TOut;
  private ByteArrayInputStream TIn;
  private final PrintStream SOut = System.out;
  private final InputStream SIn = System.in;
  String[] args = {};

  @Test
  public void testPart1a(){
    ItemToPurchase test = new ItemToPurchase("Bottled Water", "Deer Park, 12 oz.", 1, 10);
    boolean result = false;
    if(test.getName()=="Bottled Water"&&
    test.getDescription()=="Deer Park, 12 oz."&&
    test.getPrice()==1&&test.getQuantity()==10){
      result = true;
    }
    String errMsg = "Item improperly initialized.\ngetName() returned "+test.getName()+"\ngetDescription() returned "+test.getDescription()+"\ngetPrice() returned "+test.getPrice()+"\ngetQuantity() returned "+test.getQuantity()+"\nCheck test for expected output.";
    assertTrue(errMsg, result);
  }
  @Test
  public void testPart1b(){
    ItemToPurchase item = new ItemToPurchase();
    item.setDescription("Volt color, Weightlifting shoes");
    String result = item.getDescription();
    assertEquals("Volt color, Weightlifting shoes", result);
  }
  @Test
  public void testPart2a(){
    ShoppingCart test = new ShoppingCart();
    boolean result = false;
    if(test.getCustomerName()=="none"&&
    test.getDate().equals("January 1, 2016")){
      result = true;
    }
    String errMsg = "Shopping Cart default constructor initialized incorrectly.\ngetCoustomerName() returned "+test.getCustomerName()+"\ngetDate() returned "+test.getDate();
    assertTrue(errMsg,result);
  }
  @Test
  public void testPart2b(){
    ShoppingCart test = new ShoppingCart("John Doe", "February 1, 2016");
    boolean result = false;
    if(test.getCustomerName()=="John Doe"&&
    test.getDate().equals("February 1, 2016")){
      result = true;
    }
    String errMsg = "Shopping Cart initialized incorrectly.\ngetCoustomerName() returned "+test.getCustomerName()+"\ngetDate() returned "+test.getDate()+"\nCheck test for expected output.";
    assertTrue(errMsg,result);
  }
  @Test
  public void testPart2c(){
    ShoppingCart cart = new ShoppingCart();
    ItemToPurchase item = new ItemToPurchase();
    for(int i=0; i<6;i++){
      item = new ItemToPurchase();
      item.setQuantity(1);
      cart.addItem(item);
    }
    int result = cart.getNumItemsInCart();
    assertEquals(6, result);
  }
  @Test
  public void testPart2d(){
    ShoppingCart cart = new ShoppingCart();
    ItemToPurchase item1 = new ItemToPurchase();
    ItemToPurchase item2 = new ItemToPurchase();

    item1.setPrice(2);
    item1.setQuantity(4);
    cart.addItem(item1);

    item2.setPrice(1);
    item2.setQuantity(1);
    cart.addItem(item2);

    int result = cart.getCostOfCart();
    assertEquals(9, result);
  }
  @Test
  public void testPart3(){
    setInput("John Doe\nFebruary 1, 2016\nq\n");
    student.main(args);
    String correct = "Enter Customer's Name:\nEnter Today's Date:\n\nCustomer Name: John Doe\nToday's Date: February 1, 2016\n\n";
    String result = getOutput().substring(0,correct.length());
    assertEquals(correct,result);
  }
  @Test
  public void testPart4(){
    setInput("John Doe\nFebruary 1, 2016\nf\ns\nq\n");
    student.main(args);
    String correct = "Enter Customer's Name:\nEnter Today's Date:\n\nCustomer Name: John Doe\nToday's Date: February 1, 2016\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\nChoose an option:\nChoose an option:\n";
    String result = getOutput();
    assertEquals(correct,result);
  }
  @Test
  public void testPart5(){
    setInput("John Doe\nFebruary 1, 2016\no\nq\n");
    student.main(args);
    String correct = "Enter Customer's Name:\nEnter Today's Date:\n\nCustomer Name: John Doe\nToday's Date: February 1, 2016\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "OUTPUT SHOPPING CART\nJohn Doe's Shopping Cart - February 1, 2016\nNumber of Items: 0\n\nSHOPPING CART IS EMPTY\n\nTotal: $0\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    String result = getOutput();
    assertEquals(correct,result);
  }
  @Test
  public void testPart7(){
    setInput("John Doe\nFebruary 1, 2016\na\nNike Romaleos\nVolt color, Weightlifting shoes\n189\n2\no\nq\n");
    student.main(args);
    String correct = "Enter Customer's Name:\nEnter Today's Date:\n\nCustomer Name: John Doe\nToday's Date: February 1, 2016\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "ADD ITEM TO CART\nEnter the item name:\nEnter the item description:\nEnter the item price:\nEnter the item quantity:\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "OUTPUT SHOPPING CART\nJohn Doe's Shopping Cart - February 1, 2016\nNumber of Items: 2\n\nNike Romaleos 2 @ $189 = $378\n\nTotal: $378\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    String result = getOutput();
    assertEquals(correct,result);
  }
  @Test
  public void testPart8a(){
    setInput("John Doe\nFebruary 1, 2016\na\nNike Romaleos\nVolt color, Weightlifting shoes\n189\n2\nd\nSpectre DVD\nq\n");
    student.main(args);
    String correct = "Enter Customer's Name:\nEnter Today's Date:\n\nCustomer Name: John Doe\nToday's Date: February 1, 2016\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "ADD ITEM TO CART\nEnter the item name:\nEnter the item description:\nEnter the item price:\nEnter the item quantity:\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "REMOVE ITEM FROM CART\nEnter name of item to remove:\nItem not found in cart. Nothing removed.\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    String result = getOutput();
    assertEquals(correct,result);
  }
  @Test
  public void testPart8b(){
    setInput("John Doe\nFebruary 1, 2016\na\nNike Romaleos\nVolt color, Weightlifting shoes\n189\n2\na\nChocolate Chips\nSemi-sweet\n3\n5\na\nPowerbeats 2 Headphones\nBluetooth headphones\n128\n1\nd\nChocolate Chips\no\nq\n");
    student.main(args);
    String correct = "Enter Customer's Name:\nEnter Today's Date:\n\nCustomer Name: John Doe\nToday's Date: February 1, 2016\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "ADD ITEM TO CART\nEnter the item name:\nEnter the item description:\nEnter the item price:\nEnter the item quantity:\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "ADD ITEM TO CART\nEnter the item name:\nEnter the item description:\nEnter the item price:\nEnter the item quantity:\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "ADD ITEM TO CART\nEnter the item name:\nEnter the item description:\nEnter the item price:\nEnter the item quantity:\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "REMOVE ITEM FROM CART\nEnter name of item to remove:\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    correct += "OUTPUT SHOPPING CART\nJohn Doe's Shopping Cart - February 1, 2016\nNumber of Items: 3\n\nNike Romaleos 2 @ $189 = $378\nPowerbeats 2 Headphones 1 @ $128 = $128\n\nTotal: $506\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    String result = getOutput();
    assertEquals(correct,result);
  }
  @Test
  public void testPart9a(){
    setInput("John Doe\nFebruary 1, 2016\na\nNike Romaleos\nVolt color, Weightlifting shoes\n189\n2\na\nChocolate Chips\nSemi-sweet\n3\n5\na\nPowerbeats 2 Headphones\nBluetooth headphones\n128\n1\nc\nThermos Stainless Steel King\n5\nq\n");
    student.main(args);
    String correct = "CHANGE ITEM QUANTITY\nEnter the item name:\nEnter the new quantity:\nItem not found in cart. Nothing modified.\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    String result = getOutput();
    result = result.substring(result.length()-correct.length(),result.length());
    assertEquals(correct,result);
  }
  @Test
  public void testPart9b(){
    setInput("John Doe\nFebruary 1, 2016\na\nNike Romaleos\nVolt color, Weightlifting shoes\n189\n2\na\nChocolate Chips\nSemi-sweet\n3\n5\na\nPowerbeats 2 Headphones\nBluetooth headphones\n128\n1\nc\nNike Romaleos\n3\no\nq\n");
    student.main(args);
    String correct = "OUTPUT SHOPPING CART\nJohn Doe's Shopping Cart - February 1, 2016\nNumber of Items: 9\n\nNike Romaleos 3 @ $189 = $567\nChocolate Chips 5 @ $3 = $15\nPowerbeats 2 Headphones 1 @ $128 = $128\n\nTotal: $710\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    String result = getOutput();
    result = result.substring(result.length()-correct.length(),result.length());
    assertEquals(correct,result);
  }
  @Test
  public void testPart6(){
    setInput("John Doe\nFebruary 1, 2016\na\nNike Romaleos\nVolt color, Weightlifting shoes\n189\n2\na\nChocolate Chips\nSemi-sweet\n3\n5\na\nPowerbeats 2 Headphones\nBluetooth headphones\n128\n1\ni\nq\n");
    student.main(args);
    String correct = "OUTPUT ITEMS' DESCRIPTIONS\nJohn Doe's Shopping Cart - February 1, 2016\n\nItem Descriptions\nNike Romaleos: Volt color, Weightlifting shoes\nChocolate Chips: Semi-sweet\nPowerbeats 2 Headphones: Bluetooth headphones\n\n";
    correct += "MENU\na - Add item to cart\nd - Remove item from cart\nc - Change item quantity\ni - Output items' descriptions\no - Output shopping cart\nq - Quit\n\n";
    correct += "Choose an option:\n";
    String result = getOutput();
    result = result.substring(result.length()-correct.length(),result.length());
    assertEquals(correct,result);
  }
  //Set up methods
  @Before
  public void setOutput(){
    TOut = new ByteArrayOutputStream();
    System.setOut(new PrintStream(TOut));
  }
  private void setInput(String data){
    TIn = new ByteArrayInputStream(data.getBytes());
    System.setIn(TIn);
  }
  private String getOutput(){
    return TOut.toString();
  }
  @After
  public void restoreSystem(){
    System.setOut(SOut);
    System.setIn(SIn);
  }
}
